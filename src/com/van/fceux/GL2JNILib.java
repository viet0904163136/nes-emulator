/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.van.fceux;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.List;
import java.util.zip.CRC32;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.ByteArrayBuffer;

import com.van.fceux2.MainActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.widget.Toast;

// Wrapper for native library

public class GL2JNILib {

     static 
     {
         System.loadLibrary("nes");
     }

    /**
     * @param width the current view width
     * @param height the current view height
     */
     
     public static int width, height;
     
     public static native void init(AssetManager assetManager, int width, int height, String strNesFile, String strAutoSaveFile, boolean resetGame, boolean loadAutoSave);
     public native void step();
     public native void onTouchDown(int touchid, int x, int y);
     public native void onTouchMove(int touchid, int x, int y);
     public native void onTouchUp(int touchid, int x, int y);
     public native void saveState(String strFile);
     public native void setPause(boolean pause, String strState);
     public native boolean onKeyDown(int keyCode);
     public native boolean onKeyUp(int keyCode);
     public static native void reset();
     
     public static int getImageWidthInAsset(String strFileName)
     {
		try 
		{
			InputStream is;
			AssetManager assetManager = MainActivity.sInstance.getAssets();
			is = assetManager.open(strFileName);
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(is, null, options);
			is.close();
			return options.outWidth;
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
			return -1;
		}
     }
     
     public static int getImageHeightInAsset(String strFileName)
     {
    	try
 		{
 			InputStream is;
 			AssetManager assetManager = MainActivity.sInstance.getAssets();
 			is = assetManager.open(strFileName);
 			BitmapFactory.Options options = new BitmapFactory.Options();
 			options.inJustDecodeBounds = true;
 			BitmapFactory.decodeStream(is, null, options);
 			is.close();
 			return options.outHeight;
 		} 
 		catch (IOException e) 
 		{
 			e.printStackTrace();
 			return -1;
 		}
     }
     
     public static int[] getImageFromAsset(String strFileName)
     {
    	try
  		{
  			InputStream is;
  			AssetManager assetManager = MainActivity.sInstance.getAssets();
  			is = assetManager.open(strFileName);
  			Bitmap bm = BitmapFactory.decodeStream(is);
  			is.close();
  			int[] ret = new int[bm.getWidth() * bm.getHeight()];
  			bm.getPixels(ret, 0, bm.getWidth(), 0, 0, bm.getWidth(), bm.getHeight());
  			return ret;
  		} 
  		catch (IOException e) 
  		{
  			e.printStackTrace();
  			return null;
  		}
     }
     
     public static long crc32(int[] array)
     {
         CRC32 c = new CRC32();
         for (int i : array)
         {
             c.update(i);
         }
         return c.getValue();
     }
     
 	public static String getExternalStorageDirectory()
 	{
 		return Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/" + MainActivity.sInstance.getPackageName();
 	}
 	
 	public static void startSaveStateActivity(String strPrefix)
 	{
 		Intent oIntent = new Intent(MainActivity.sInstance, SaveStateActivity.class);
 		oIntent.putExtra("prefix", strPrefix);
 		MainActivity.sInstance.startActivityForResult(oIntent, 1);
 	}
 	
 	public static void startLoadStateActivity(String strPrefix)
 	{
 		Intent oIntent = new Intent(MainActivity.sInstance, LoadStateActivity.class);
 		oIntent.putExtra("prefix", strPrefix);
 		MainActivity.sInstance.startActivityForResult(oIntent, 2);
 	}
 	
    public static void showResetDialog()
    {
    	final Handler h = new Handler();
    	AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.sInstance);
		builder.setTitle("Reset");
		builder.setMessage("Do you really want to reset game?");
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface dialog, int which) {
				//setUserWantReset();
				//reset();
				//init(AssetManager assetManager, int width, int height, String strNesFile, String strAutoSaveFile, boolean resetGame, boolean loadAutoSave);
				h.post(new Runnable(){

					@Override
					public void run() {
						SelectGameActivity.sInstance.resetGame = true;
						MainActivity.sInstance.finish();
					}
					
				});
	        	
			}});
		builder.setNegativeButton("No", new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
			}});
		builder.create().show();
    }
 	
	@SuppressLint("NewApi")
	public static void ExecuteAsyncTask(AsyncTask<String, ?, ?> oTask, String... params)
	{
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
		{
			oTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
		}
		else
		{
			oTask.execute(params);
		}
	}

}




class DownloadResult {
	public String url;
	public String content = "";
	public boolean NetworkError = false;
	public DownloadResult(String strUrl)
	{
		url = strUrl;
	}
}

class HtmlDownloader extends AsyncTask<String, Void, DownloadResult> {
	
	@Override
	protected DownloadResult doInBackground(String... args) {
		DownloadResult oRet = new DownloadResult(args[0]);
		ByteArrayBuffer oBAF = new ByteArrayBuffer(64);
		try
		{
			HttpClient oHttpClient = new DefaultHttpClient();
			HttpContext oLocalContext = new BasicHttpContext();
			HttpGet oHttpGet = new HttpGet(args[0]);
			HttpResponse oResponse = oHttpClient.execute(oHttpGet, oLocalContext);
			InputStream oIS = oResponse.getEntity().getContent();
			BufferedInputStream oBIS = new BufferedInputStream(oIS);
			byte[] temp = new byte[64];
			
			while(true)
			{
				int n = oBIS.read(temp);
				if(n == -1)
					break;
				if(n > 0)
					oBAF.append(temp, 0, n);
			}
		}
		catch(OutOfMemoryError e)
		{
			return oRet;
		}
		catch(EOFException e)
		{
			oRet.content = new String(oBAF.toByteArray());
			return oRet;
		}
		catch(UnknownHostException e)
		{
			oRet.NetworkError = true;
			return oRet;
		}
		catch(Exception e)
		{
			return oRet;
		}
		oRet.content = new String(oBAF.toByteArray());
		return oRet;
	}

	@Override
	protected void onPostExecute(DownloadResult obj)
	{
		String strTemp = obj.content;
		while(true)
		{
			int n = strTemp.indexOf("<a ");
			if(n >= 0)
			{
				strTemp = strTemp.substring(n);
				n = strTemp.indexOf("href=");
				strTemp = strTemp.substring(n + 6);
				n = strTemp.indexOf('"');
				String strLink = strTemp.substring(0, n);
				if(strLink.endsWith(".jpg") || strLink.endsWith(".png"))
				{
					Toast.makeText(MainActivity.sInstance, strLink, Toast.LENGTH_LONG).show();
				}
			}
			else
			{
				break;
			}
		}
	}
}