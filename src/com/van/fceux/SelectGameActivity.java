package com.van.fceux;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipInputStream;

import com.van.fceux2.MainActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import com.van.fceux2.R;

public class SelectGameActivity extends Activity {
	
	List<String> mFoundFiles = new ArrayList<String>();
	String strFile = "";
	String strAutoSaveFile = "";
	public static SelectGameActivity sInstance = null;
	public static boolean resetGame = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sInstance = this;
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		this.setContentView(R.layout.select_game);
		
		if(!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
		{
			Toast.makeText(this, "This app required a SD card!", Toast.LENGTH_LONG).show();
		}
		
		String strTempPath = Environment.getExternalStorageDirectory().getAbsolutePath();
		strTempPath += "/Android/data/";
		strTempPath += SelectGameActivity.this.getPackageName();
		File dir = new File(strTempPath);
		dir.mkdirs();
		
		EditText et = (EditText)findViewById(R.id.editText1);
		et.setOnEditorActionListener(new OnEditorActionListener(){
			@Override
			public boolean onEditorAction(TextView arg0, int arg1, KeyEvent arg2) {
				openSearchPage();
				return false;
			}});
		
		final Button searchButton = (Button)this.findViewById(R.id.Search);
		searchButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				openSearchPage();
			}
		});
		
		final Button oButton = (Button)this.findViewById(R.id.ScanSDCard);
		oButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				ScanSDCardTask scanThread = new ScanSDCardTask(SelectGameActivity.this, (ListView)SelectGameActivity.this.findViewById(R.id.ListFiles), oButton,
						(TextView)SelectGameActivity.this.findViewById(R.id.StatusText),
						(ProgressBar)SelectGameActivity.this.findViewById(R.id.progressBar1));
				scanThread.execute();
			}
		});

		mFoundFiles = getCachedRomFiles();
		if(mFoundFiles != null && mFoundFiles.size() > 0)
		{
			ListView lv = (ListView) SelectGameActivity.this
					.findViewById(R.id.ListFiles);
			MyListAdapter adapter = new MyListAdapter(this,
					R.layout.file_view, mFoundFiles);
			lv.setAdapter(adapter);
		}
		else
		{
			ScanSDCardTask scanThread = new ScanSDCardTask(SelectGameActivity.this, (ListView)SelectGameActivity.this.findViewById(R.id.ListFiles), oButton,
					(TextView)SelectGameActivity.this.findViewById(R.id.StatusText),
					(ProgressBar)SelectGameActivity.this.findViewById(R.id.progressBar1));
			scanThread.execute();
		}
		
		ListView lv = (ListView)SelectGameActivity.this.findViewById(R.id.ListFiles);
		lv.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) 
			{
				try
				{
					strFile = arg0.getItemAtPosition(arg2).toString();
					String strExtension = getExtension(strFile);
					if(strExtension.compareTo("zip") == 0)
					{
						String strNesFile = getNesFileInZip(strFile);
						InputStream is;
						ZipInputStream zis;
						is = new FileInputStream(strFile);
						zis = new ZipInputStream(new BufferedInputStream(is));
						ZipEntry ze;
						while((ze = zis.getNextEntry()) != null)
						{
							if(ze.getName().compareTo(strNesFile) == 0)
							{
								String strTempPath = Environment.getExternalStorageDirectory().getAbsolutePath();
								strTempPath += "/Android/data/";
								strTempPath += SelectGameActivity.this.getPackageName();
								File dir = new File(strTempPath);
								dir.mkdirs();
								FileOutputStream fout = new FileOutputStream(strTempPath + "/" + strNesFile);
								byte[] buffer = new byte[1024];
								int count = 0;
								while((count = zis.read(buffer)) != -1)
								{
									fout.write(buffer, 0, count);
								}
								fout.close();
							}
							zis.closeEntry();
						}
						zis.close();
						strFile = Environment.getExternalStorageDirectory().getAbsolutePath() +
									"/Android/data/" +
									SelectGameActivity.this.getPackageName() + "/" + strNesFile;
					}
					
					File file = new File(strFile);
					long fileLength = file.length();
					byte[] data = new byte[(int) fileLength];
					int[] temp = new int[(int)fileLength];
					FileInputStream fis = new FileInputStream(strFile);
					BufferedInputStream bis = new BufferedInputStream(fis);
					bis.read(data);
					for(int i=0; i<data.length; i++)
					{
						temp[i] = data[i];
					}
					long crc32 = GL2JNILib.crc32(temp);
					bis.close();
					fis.close();
					
					String strFileNameOnly = strFile.substring(strFile.lastIndexOf('/') + 1);
					strAutoSaveFile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/" + SelectGameActivity.this.getPackageName() + "/" + strFileNameOnly + "_" + crc32 + ".autosave";
					

					File autoSaveFile = new File(strAutoSaveFile);
					if(autoSaveFile.exists())
					{
//						AlertDialog.Builder builder = new AlertDialog.Builder(SelectGameActivity.this);
//						builder.setTitle(strFileNameOnly);
//						builder.setMessage("Continue from last exit?");
//						builder.setCancelable(true);
//						builder.setPositiveButton("Yes", new DialogInterface.OnClickListener(){
//							@Override
//							public void onClick(DialogInterface dialog, int which) {
								Intent oIntent = new Intent(SelectGameActivity.this, MainActivity.class);
								oIntent.putExtra("file", strFile);
								oIntent.putExtra("autosave", strAutoSaveFile);
								oIntent.putExtra("resetGame", false);
								MainActivity.loadAutoSave = true;
								SelectGameActivity.this.startActivityForResult(oIntent, 1);
//							}});
//						builder.setNegativeButton("No", new DialogInterface.OnClickListener(){
//							@Override
//							public void onClick(DialogInterface arg0, int arg1) {
//								Intent oIntent = new Intent(SelectGameActivity.this, MainActivity.class);
//								oIntent.putExtra("file", strFile);
//								oIntent.putExtra("autosave", strAutoSaveFile);
//								oIntent.putExtra("resetGame", true);
//								MainActivity.loadAutoSave = false;
//								SelectGameActivity.this.startActivityForResult(oIntent, 1);
//							}});
//						builder.create().show();
					}
					else
					{
						Intent oIntent = new Intent(SelectGameActivity.this, MainActivity.class);
						oIntent.putExtra("file", strFile);
						oIntent.putExtra("autosave", strAutoSaveFile);
						oIntent.putExtra("resetGame", true);
						SelectGameActivity.this.startActivityForResult(oIntent, 1);
					}
				}
				catch(Exception ex)
				{
					
				}
			}});
	}
	
	void openSearchPage()
	{
		EditText tv = (EditText)findViewById(R.id.editText1);
		String strRom = tv.getText().toString().trim();
		if(strRom.compareTo("") == 0)
		{
			strRom = "best+nes+games";
			Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("http://www.google.com/search?q=" + strRom));
			SelectGameActivity.this.startActivity(intent);
			return;
		}
		else
		{
			while(strRom.contains("  "))
			{
				strRom = strRom.replace("  ", " ");
			}
			strRom += " nes";
			strRom = strRom.replace(' ', '+');
			Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("http://www.google.com/search?q=" + strRom));
			SelectGameActivity.this.startActivity(intent);
		}
	}

	private List<String> getCachedRomFiles()
	{
		List<String> ret = new ArrayList<String>();
		FileReader fr = null;
		BufferedReader br = null;
		try
		{
			File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + 
					"/Android/data/" + this.getApplicationContext().getPackageName() + 
					"/listrom.txt");
			
			if(f.exists())
			{
				fr = new FileReader(
						Environment.getExternalStorageDirectory().getAbsolutePath() + 
						"/Android/data/" + this.getApplicationContext().getPackageName() + 
						"/listrom.txt");
				
				br = new BufferedReader(fr);
				while(true)
				{
					String strFile = br.readLine();
					if(strFile != null && strFile.compareTo("") != 0)
					{
						File file = new File(strFile);
						if(file.exists())
							ret.add(strFile);
					}
					else
					{
						break;
					}
				}
				
			}
		}
		catch(Exception ex)
		{
			
		}
		finally
		{
			try
			{
				br.close();
				fr.close();
			} 
			catch (Exception e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		return ret;
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if(requestCode == 1)
		{
			if(resetGame)
			{
				resetGame = false;
				Timer t = new Timer();
				t.schedule(new TimerTask(){

					@Override
					public void run() {
						Intent oIntent = new Intent(SelectGameActivity.this, MainActivity.class);
						oIntent.putExtra("file", strFile);
						oIntent.putExtra("autosave", strAutoSaveFile);
						oIntent.putExtra("resetGame", true);
						MainActivity.loadAutoSave = false;
						SelectGameActivity.this.startActivityForResult(oIntent, 1);
					}}, 500);
			}
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		return true;
	}
	
	public static String getNesFileInZip(String strZipFile)
	{
		File f = new File(strZipFile);
		return getNesFileInZip(f);
	}
	
	public static String getNesFileInZip(File f)
	{
		try 
		{
			java.util.zip.ZipFile zip;
			zip = new java.util.zip.ZipFile(f);
			Enumeration<? extends ZipEntry> e = zip.entries();
			while(e.hasMoreElements())
			{
				String strName = e.nextElement().getName();
				String strExtension = getExtension(strName);
				if(strExtension.compareTo("nes") == 0)
				{
					zip.close();
					return strName;
				}
			}
			zip.close();
		} 
		catch (ZipException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (IOException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return "";
	}
	
	public static String getExtension(String strFileName)
	{
		int n = strFileName.lastIndexOf('.');
		if(n >= 0)
		{
			return strFileName.substring(n + 1).toLowerCase();
		}
		return "";
	}
}

class MyListAdapter extends ArrayAdapter<String>
{
	Context context;
	int id;
	List<String> items;
	
	public MyListAdapter(Context context, int resource, List<String> objects) {
		super(context, resource, objects);
		this.context = context;
		id = resource;
		items = objects;
	}
	
	@Override
	public View getView(int position, View viewConvert, ViewGroup parent)
	{
		View v = viewConvert;
		if(v == null)
		{
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(id, null);
			TextView tv1 = (TextView)v.findViewById(R.id.textView1);
			TextView tv2 = (TextView)v.findViewById(R.id.textView2);
			String strPath = items.get(position);
			int n = strPath.lastIndexOf('/');
			String strFileName = strPath;
			if(n >= 0)
			{
				strFileName = strPath.substring(n + 1);
			}
			tv1.setText(strFileName);
			tv2.setText(strPath);
		}
		return v;
	}
}

class ScanSDCardTask extends AsyncTask<Void, Void, Void>
{
	Context context;
	List<String> mFoundFiles = new ArrayList<String>();
	ListView mLV;
	Button mScanButton;
	TextView mStatusText;
	ProgressBar mProgressBar;
	
	public ScanSDCardTask(Context context, ListView lv, Button ScanButton, TextView statusView, ProgressBar progressBar)
	{
		mLV = lv;
		this.context = context;
		mScanButton = ScanButton;
		mStatusText = statusView;
		mProgressBar = progressBar;
	}
	
	@Override
	protected void onPreExecute()
	{
		mScanButton.setText("Scanning...");
		mStatusText.setText("Please Wait...");
		mStatusText.setVisibility(View.VISIBLE);
		mProgressBar.setVisibility(View.VISIBLE);
		mScanButton.setVisibility(View.GONE);
	}
	
	@Override
	protected Void doInBackground(Void... arg0)
	{
		try
		{
			mFoundFiles.clear();
			String strSDCard = Environment.getExternalStorageDirectory().getAbsolutePath();
			File dir = new File(strSDCard);
			List<File> listDir = new ArrayList<File>();
			listDir.add(dir);
			while(listDir.size() > 0)
			{
				try
				{
					dir = listDir.get(0);
					listDir.remove(0);
					File[] listFile = dir.listFiles();
					if(listFile != null)
					{
						for(int i=0; i<listFile.length; i++)
						{
							if(listFile[i].isDirectory())
							{
								if(!(listFile[i].getAbsolutePath().endsWith("Android/data") ||
										listFile[i].getAbsolutePath().endsWith("Android/data/") ||
										listFile[i].getAbsolutePath().endsWith("Android/obb") ||
										listFile[i].getAbsolutePath().endsWith("Android/obb/")))
									listDir.add(listFile[i]);
							}
							else
							{
								int n = listFile[i].getName().lastIndexOf('.');
								if(n >= 0)
								{
									String strExtension = listFile[i].getName().substring(n + 1).toLowerCase();
									if(strExtension.compareTo("nes") == 0)
									{
										mFoundFiles.add(listFile[i].getAbsolutePath());
									}
									else if(strExtension.compareTo("zip") == 0)
									{
										try
										{
											boolean bIsNesFile = false;
											java.util.zip.ZipFile zip = new java.util.zip.ZipFile(listFile[i]);
											Enumeration<? extends ZipEntry> e = zip.entries();
											while(e.hasMoreElements())
											{
												String strName = e.nextElement().getName();
												n = strName.lastIndexOf('.');
												if(n >= 0)
												{
													strExtension = strName.substring(n + 1).toLowerCase();
													if(strExtension.compareTo("nes") == 0)
													{
														bIsNesFile = true;
														break;
													}
												}
											}
											
											if(bIsNesFile)
											{
												mFoundFiles.add(listFile[i].getAbsolutePath());
											}
											
										} catch (ZipException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										} catch (IOException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										
									}
								}
							}
						}
					}
				}
				catch (Exception ex)
				{
					
				}
			}
		}
		catch(Exception ex)
		{
			
		}
		
		return null;
	}
	
	@Override
	protected void onPostExecute(Void arg)
	{
		try
		{
			mScanButton.setText("Scan SDCard");
			MyListAdapter adapter = new MyListAdapter(context, R.layout.file_view, mFoundFiles);
			mLV.setAdapter(adapter);
			
			if(mFoundFiles.size() > 0)
			{
				FileWriter fw = new FileWriter(
						Environment.getExternalStorageDirectory().getAbsolutePath() + 
						"/Android/data/" + context.getApplicationContext().getPackageName() + 
						"/listrom.txt"
						);
				
				for(int i=0; i<mFoundFiles.size(); i++)
					fw.write(mFoundFiles.get(i) + "\n");
				
				fw.flush();
				fw.close();
				mStatusText.setVisibility(View.GONE);
			}
			else
			{
				mStatusText.setText("Can not find any ROM games in your SD card. You must copy your own ROM files into SD card!");
				mStatusText.setVisibility(View.VISIBLE);
			}
			
			mProgressBar.setVisibility(View.GONE);
			mScanButton.setVisibility(View.VISIBLE);
		}
		catch (Exception ex)
		{
			
		}
	}
}