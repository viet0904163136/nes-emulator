package com.van.fceux2;

import java.util.Timer;
import java.util.TimerTask;

import com.van.fceux2.R;
import com.van.fceux.GL2JNIView;
import com.van.fceux2.MainActivity;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class MainActivity extends Activity {

    static 
    {
        System.loadLibrary("nes");
    }
    
    GL2JNIView mView;
	public static MainActivity sInstance = null;
	public static Handler sHander = new Handler();
	public static String sNesFile = "";
	public static String saveStateSlot = "";
	public static String autosaveFile = "";
	public static boolean resetGame = false;
	public static boolean loadAutoSave = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sInstance = this;
		
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		Intent oIntent = this.getIntent();
		sNesFile = oIntent.getStringExtra("file");
		autosaveFile = oIntent.getStringExtra("autosave");
		resetGame = oIntent.getBooleanExtra("resetGame", false);
		
		this.setContentView(R.layout.main);
		
		mView = new GL2JNIView(getApplication());
		ViewGroup gameContainer = (ViewGroup)this.findViewById(R.id.gameContainer);
		gameContainer.addView(mView);
		
		final AdView adView = (AdView)this.findViewById(R.id.adView);// new AdView(this, AdSize.BANNER, "a152f241d7cfb80");
		AdRequest request = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).
                addTestDevice("895642C3F89AFDA819C00BF70EC1ECD6").build();              
        adView.loadAd(request);
		
		final Handler h = new Handler();
		
		final Button oCloseAdsButton = (Button)this.findViewById(R.id.ButtonCloseAds);
		if(oCloseAdsButton != null)
		{
			oCloseAdsButton.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View arg0) {
					adView.setVisibility(View.GONE);
					oCloseAdsButton.setVisibility(View.GONE);
					
					Timer t = new Timer();
					t.schedule(new TimerTask(){

						@Override
						public void run() {
							h.post(new Runnable(){
								@Override
								public void run() {
									adView.setVisibility(View.VISIBLE);
									oCloseAdsButton.setVisibility(View.VISIBLE);		
								}});
						}}, 600000);
				}});
		}
		
		adView.setAdListener(new AdListener(){
			@Override
			public void onAdLoaded()
			{
				if(oCloseAdsButton != null)
					oCloseAdsButton.setVisibility(View.VISIBLE);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if(requestCode == 1 || requestCode == 2)
		{
			mView.mRenderer.mGL2JNILib.setPause(false, saveStateSlot);
		}
	}
	
	@Override 
	public void onSaveInstanceState(Bundle outState)
	{
		//Toast.makeText(this, "need save:" + autosaveFile, 0).show();
		mView.mRenderer.mGL2JNILib.saveState(autosaveFile);
	}

	@Override
	public void onBackPressed()
	{
		//Toast.makeText(this, "need save:" + autosaveFile , 0).show();
		mView.mRenderer.mGL2JNILib.saveState(autosaveFile);
		finish();
	}
}
