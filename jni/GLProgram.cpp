#include "GLProgram.h"
#include "Log.h"

VGLProgram::VGLProgram()
{
	mVsId = 0;
	mFsId = 0;
	mProgramId = 0;
}

VGLProgram::~VGLProgram()
{
	unload();
}

#ifdef ANDROID
void VGLProgram::loadFromAsset(const std::string& strVsFileName, const std::string& strFsFileName)
{
	std::string strVs = VAssetManager::readAsString(strVsFileName);
	std::string strFs = VAssetManager::readAsString(strFsFileName);
	loadFromString(strVs, strFs);
}
#endif

void VGLProgram::loadFromString(const std::string& strVs, const std::string& strFs)
{
    mVsId = loadShader(GL_VERTEX_SHADER, strVs);
    if (!mVsId)
    {
    	std::stringstream ss;
		ss << "Can not load vertex shader:\n" << strVs << std::endl;
		throw VException(ss.str());
    }

    mFsId = loadShader(GL_FRAGMENT_SHADER, strFs);
    if (!mFsId)
    {
    	std::stringstream ss;
    	ss << "Can not load pixel shader:\n" << strFs << std::endl;
        throw VException(ss.str());
    }

    mProgramId = glCreateProgram();
	if (mProgramId)
	{
		glAttachShader(mProgramId, mVsId);
		CHECKGLERROR;
		glAttachShader(mProgramId, mFsId);
		CHECKGLERROR;
		glLinkProgram(mProgramId);
		GLint linkStatus = GL_FALSE;
		glGetProgramiv(mProgramId, GL_LINK_STATUS, &linkStatus);
		if (linkStatus != GL_TRUE)
		{
			GLint bufLength = 0;
			glGetProgramiv(mProgramId, GL_INFO_LOG_LENGTH, &bufLength);
			if (bufLength)
			{
				char* buf = (char*) malloc(bufLength);
				if (buf)
				{
					glGetProgramInfoLog(mProgramId, bufLength, NULL, buf);
					std::stringstream ss;
					ss << "Could not link program:\n" << buf << std::endl;
					throw VException(ss.str());
					free(buf);
				}
			}
			glDeleteProgram(mProgramId);
			mProgramId = 0;
		}
	}
}

GLuint VGLProgram::loadShader(int nType, const std::string& strShader)
{
	GLuint shader = glCreateShader(nType);
	if (shader)
	{
		const char* strSource = strShader.c_str();
		glShaderSource(shader, 1, &strSource, NULL);
		glCompileShader(shader);
		GLint compiled = 0;
		glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
		if (!compiled)
		{
			GLint infoLen = 0;
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
			if (infoLen)
			{
				char* buf = (char*) malloc(infoLen);
				if (buf)
				{
					glGetShaderInfoLog(shader, infoLen, NULL, buf);
					LOGE("Could not compile shader %d:\n%s\n", nType, buf);
					free(buf);
				}
				glDeleteShader(shader);
				shader = 0;
			}
		}
	}
	return shader;
}

int VGLProgram::getAttributeId(const std::string& strAttrName)
{
	int nAttrId = glGetAttribLocation(mProgramId, strAttrName.c_str());
	CHECKGLERROR;
	return nAttrId;
}

int VGLProgram::getUniformId(const std::string& strUniformName)
{
	int nUniformId = glGetUniformLocation(mProgramId, strUniformName.c_str());
	CHECKGLERROR;
	return nUniformId;
}

void VGLProgram::use()
{
	if(mProgramId)
	{
		glUseProgram(mProgramId);
		CHECKGLERROR;
	}
	else
	{
		throw VException("Program was not loaded");
	}
}

void VGLProgram::bindVector(const std::string& strAttrName, int size, const void* data, GLsizei stride /*= 0*/)
{
	int nAttrId = getAttributeId(strAttrName);

	glVertexAttribPointer(nAttrId, size, GL_FLOAT, GL_FALSE, stride, data);
	CHECKGLERROR;
	glEnableVertexAttribArray(nAttrId);
	CHECKGLERROR;
}

void VGLProgram::bindVector2(const std::string& strAttrName, const void* data, int stride /*= 0*/)
{
	bindVector(strAttrName, 2, data, stride);
}

void VGLProgram::bindVector3(const std::string& strAttrName, const void* data, GLsizei stride /*= 0*/)
{
	bindVector(strAttrName, 3, data, stride);
}

void VGLProgram::bindVector4(const std::string& strAttrName, const void* data, GLsizei stride /*= 0*/)
{
	bindVector(strAttrName, 4, data, stride);
}

bool VGLProgram::isLoaded()
{
	return (mProgramId != 0);
}

void VGLProgram::unload()
{
	if(mProgramId)
	{
		//glDeleteProgram(mProgramId);
		mProgramId = 0;
	}

	if(mVsId)
	{
		//glDeleteShader(mVsId);
		mVsId = 0;
	}

	if(mFsId)
	{
		//glDeleteShader(mFsId);
		mFsId = 0;
	}
}
