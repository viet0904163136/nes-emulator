#include "Texture.h"
#include "Global.h"
#include <fstream>
#include "ImageLoader.h"

VGLProgram VTexture::sProgram;
list<VTexture*> VTexture::sTextureList;

VTexture::VTexture()
{
	LOGI("%s %d", __PRETTY_FUNCTION__, __LINE__);
	mTextureId = 0;
	mWidth = 0;
	mHeight = 0;
	enableBlend = true;
	alpha = 0.3f;
	renderX = renderY = renderWidth = renderHeight = 0;
	sTextureList.push_back(this);
	userdata = -1;
}

VTexture::~VTexture()
{
	LOGI("%s %d", __PRETTY_FUNCTION__, __LINE__);
	sTextureList.remove(this);
	//glDeleteTextures(1, &mTextureId);
	mTextureId = 0;
}

int VTexture::getWidth()
{
	return mWidth;
}

int VTexture::getHeight()
{
	return mHeight;
}

void VTexture::init(const std::string& strFileName)
{
	mStrFileName = strFileName;
	int width = VImageLoader::getImageWidth(strFileName);
	int height = VImageLoader::getImageHeight(strFileName);
	mData = VImageLoader::getImage(strFileName);
	this->init(width, height, mData.get());
}

void VTexture::init(int width, int height, int* data /* = NULL */)
{
	mWidth = width;
	mHeight = height;

	glGenTextures(1, &mTextureId);
	CHECKGLERROR;
	glBindTexture(GL_TEXTURE_2D, mTextureId);
	CHECKGLERROR;
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	CHECKGLERROR;
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	CHECKGLERROR;
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	CHECKGLERROR;
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	if(data)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	}
	else
	{
		shared_ptr<int> tempdata(new int[width * height], default_delete<int[]>());
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, tempdata.get());
	}
	CHECKGLERROR;
}

void VTexture::reload()
{
	if(mData != NULL)
	{
		this->init(mWidth, mHeight, mData.get());
	}
	else if(mStrFileName != "")
	{
		LOGI("reload %s", mStrFileName.c_str());
		this->init(mStrFileName);
	}
	else
	{
		if(mWidth > 0 && mHeight > 0)
		{
			this->init(mWidth, mHeight);
		}
	}
}

void VTexture::destroy()
{
	if(mTextureId)
	{
		glDeleteTextures(1, &mTextureId);
		mTextureId = 0;
		for (GLint error = glGetError(); error; error = glGetError())
		{
		}
	}

}

//void VTexture::init(int width, int height)
//{
//	glGenTextures(1, &mTextureId);
//	glBindTexture(GL_TEXTURE_2D, mTextureId);
//	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//	shared_ptr<int> tempdata(new int[width * height]);
//	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, tempdata.get());
//}

GLuint VTexture::getTextureId()
{
/*
	static int n = 0;
	static int* t = new int[256*256];
	for(int i=0; i<256; i++)
	{
		for(int j=0; j<256; j++)
		{
			t[i * 256 + j] = n;
		}
	}
	n++;

		::glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 256, 256, GL_RGBA, GL_UNSIGNED_BYTE, t);
*/
	return mTextureId;
}

void VTexture::update(int x, int y, int width, int height, int* pixels)
{
	LOGI("%s %d", __PRETTY_FUNCTION__, __LINE__);
	glBindTexture(GL_TEXTURE_2D, mTextureId);
	CHECKGLERROR;
	LOGI("%s %d", __PRETTY_FUNCTION__, __LINE__);
	glTexSubImage2D(GL_TEXTURE_2D, 0, x, y, width, height, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	CHECKGLERROR;
	LOGI("%s %d", __PRETTY_FUNCTION__, __LINE__);
	CHECKGLERROR;
	LOGI("%s %d", __PRETTY_FUNCTION__, __LINE__);
}

void VTexture::render(int x, int y, int width, int height)
{
	renderX = x;
	renderY = y;
	renderWidth = width;
	renderHeight = height;

	if(!sProgram.isLoaded())
	{
#ifdef ANDROID
		sProgram.loadFromAsset("texture_vs.txt", "texture_fs.txt");
#else
		std::ifstream fin;
		fin.open("D:\\Workspace\\NESEmulatorJNI\\jni\\FCEUX\\FCEUX\\texture_vs.txt", std::ifstream::binary);
		fin.seekg(0, fin.end);
		int len = (int)fin.tellg();
		fin.seekg(0, fin.beg);
		shared_ptr<char> data(new char[len + 1], std::default_delete<char[]>());
		data.get()[len] = 0;
		fin.read(data.get(), len);
		std::string vsSource(data.get());
		fin.close();

		fin.open("D:\\Workspace\\NESEmulatorJNI\\jni\\FCEUX\\FCEUX\\texture_fs.txt", std::ifstream::binary);
		fin.seekg(0, std::ios_base::end);
		len = (int)fin.tellg();
		fin.seekg(0, std::ios_base::beg);
		data.reset(new char[len + 1], std::default_delete<char[]>());
		data.get()[len] = 0;
		fin.read(data.get(), len);
		std::string fsSource(data.get());
		fin.close();
		sProgram.loadFromString(vsSource, fsSource);
#endif
	}

	if(Global::width == 0 || Global::height == 0)
	{
		throw VException("Global is not initialized");
	}

	float fx = (float)x / Global::width * 2.0f - 1.0f;
	float fw = (float)width / Global::width * 2.0f;
	float fy = 1.0f - ((float)y / Global::height * 2);
	float fh = (float)height / Global::height * 2.0f;

	GLfloat vertices[12];

	vertices[0 * 2 + 0] = fx; vertices[0 * 2 + 1] = fy - fh;
	vertices[1 * 2 + 0] = fx + fw; vertices[1 * 2 + 1] = fy - fh;
	vertices[2 * 2 + 0] = fx; vertices[2 * 2 + 1] = fy;

	vertices[3 * 2 + 0] = fx + fw; vertices[3 * 2 + 1] = fy - fh;
	vertices[4 * 2 + 0] = fx + fw; vertices[4 * 2 + 1] = fy;
	vertices[5 * 2 + 0] = fx; vertices[5 * 2 + 1] = fy;

	GLfloat uv[12];
	uv[0 * 2 + 0] = 0.0f; uv[0 * 2 + 1] = 1.0f;
	uv[1 * 2 + 0] = 1.0f; uv[1 * 2 + 1] = 1.0f;
	uv[2 * 2 + 0] = 0.0f; uv[2 * 2 + 1] = 0.0f;

	uv[3 * 2 + 0] = 1.0f; uv[3 * 2 + 1] = 1.0f;
	uv[4 * 2 + 0] = 1.0f; uv[4 * 2 + 1] = 0.0f;
	uv[5 * 2 + 0] = 0.0f; uv[5 * 2 + 1] = 0.0f;

//	const GLfloat gTriangleVertices[] = { -0.5f, 0.5f, -0.5f, -0.5f,
//	        0.5f, -0.5f };
//
//	const GLfloat ggg[] = {0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f};
//
	sProgram.use();
	CHECKGLERROR;
	sProgram.bindVector2("aPosition", vertices);
	CHECKGLERROR;
	sProgram.bindVector2("aTextureCoord", uv);
	CHECKGLERROR;

    glActiveTexture(GL_TEXTURE0);
	CHECKGLERROR;
	glBindTexture(GL_TEXTURE_2D, getTextureId());
	CHECKGLERROR;

	int texture = sProgram.getUniformId("uTexture");
	::glUniform1i(texture, 0);
	CHECKGLERROR;

	int uAlpha = sProgram.getUniformId("uAlpha");
	::glUniform1f(uAlpha, this->alpha);

	if(enableBlend)
	{
		glEnable (GL_BLEND);
		glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
	else
	{
		glDisable(GL_BLEND);
	}

	glDrawArrays(GL_TRIANGLES, 0, 6);
	CHECKGLERROR;

	glUseProgram(0);
}

bool VTexture::pointInRenderRegion(int x, int y)
{
	if((x >= renderX) && (y >= renderY) && (x <= (renderX + renderWidth)) && (y <= (renderY + renderHeight)))
		return true;
	return false;
}

void VTexture::notifyDeviceReset()
{
	LOGI("notify device reset");
	list<VTexture*>::iterator itor = sTextureList.begin();
	for(int i=0; i<sTextureList.size(); ++i)
	{
		(*itor)->reload();
		++itor;
	}
}
