#ifdef ANDROID
#include <android/log.h>
#endif

//#define DEBUG

#ifdef ANDROID

	#define  LOG_TAG    "libgl2jni"
	#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

	#ifdef DEBUG
		#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
	#else
		#define  LOGI(...)
	#endif

#else

#define __PRETTY_FUNCTION__ __FUNCTION__
#define  LOGI(...) printf(__VA_ARGS__)
#define  LOGE(...) printf(__VA_ARGS__)

#endif
